# Url Shortener (Grid Dynamics task)

### Task description:
```
 We’d like you to build a website that functions as a URL Shortener:
1. A user should be able to load the index page of your site and be presented with an input field where they can enter a URL.
2. Upon entering the URL, a “shortened” version of that url is created and shown to the user as a URL to the site you’re building.
3. When visiting that “shortened” version of the URL, the user is redirected to the original URL.
4. Additionally, if a URL has already been shortened by the system, and it is entered a second time, the first shortened URL should be given back to the user.

For example, if I enter http://www.apple.com/iphone/ into the input field, and I’m running the app locally on port 9000, I’d expect to be given back a URL that looked something like http://localhost:9000/1. Then when I visit http://localhost:9000/1, I am redirected to http://www.apple.com/iphone/.
```

### Solution
I decided to choose SQL database to store urls because it's more convenient way to solve this problem.
Firstly, there are no problems with duplication (because of DB constraint), not any urls collision.
Secondly, it's really easy way to convert full url to shorten one, I need just to convert primary key of url entity from 10-th notation to __another notation__ (62-th, if we count only english upper/lower case alphabet and numbers).

As I understand business logic of backend part, it might have two endpoints:
1. Put URL to DB, generate shorten one
2. Retrieve full url via shorten one.

_In case of SQL, I just need to create constraint of full url and create an index for shorten url._

### How to run it?
1. `mvn clean install`
2. `mvn spring-boot:run`
3. By default, `H2` database used for local testing, open `http://localhost:8080/h2-console/` and type `jdbc:h2:mem:testdb` for jdbc url:
   <a href="https://ibb.co/D1ZNY9f"><img src="https://i.ibb.co/vcSbh34/image.png" alt="image" border="0"></a>
4. Then open web part via `src/main/resources/html/index.html` in your favorite browser.
5. Enjoy!

### Examples
<a href="https://ibb.co/TtSD4yy"><img src="https://i.ibb.co/pd86fqq/image.png" alt="image" border="0"></a>
