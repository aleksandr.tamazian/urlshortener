package com.griddynamics.task.service

import com.griddynamics.task.model.dto.request.RequestUrlDto
import com.griddynamics.task.model.dto.response.ResponseUrlDto
import com.griddynamics.task.model.entity.UrlEntity
import com.griddynamics.task.repository.UrlRepository
import com.griddynamics.task.tools.sanitizeUrl
import com.griddynamics.task.tools.shortenUrl
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class UrlService(
        private val urlRepository: UrlRepository
) {
    fun retrieveFullUrl(shortenUrl: String): ResponseUrlDto {
        val url = urlRepository.findByShortenUrl(shortenUrl)
        return ResponseUrlDto(url.fullUrl, url.shortenUrl!!)
    }

    @Transactional
    fun addUrl(requestUrlDto: RequestUrlDto): ResponseUrlDto {
        val urlEntity = UrlEntity(null, sanitizeUrl(requestUrlDto.fullUrl), null)
        val url = urlRepository.save(urlEntity)
        val shortenUrl = shortenUrl(url.id!!)
        urlRepository.updateShortenUrl(url.id, shortenUrl)

        return ResponseUrlDto(url.fullUrl, shortenUrl)
    }
}