package com.griddynamics.task.tools

private val map = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray()

fun sanitizeUrl(originalUrl: String): String {
    // Check for BOM
    val bomCharacter = "\uFEFF"
    return if (originalUrl.startsWith(bomCharacter)) {
        originalUrl.substring(bomCharacter.length).trim()
    } else {
        originalUrl.trim()
    }
}

fun shortenUrl(n: Long): String {
    var num = n
    val shortUrl = StringBuffer()

    // Convert given integer id to a base 62 number
    while (num > 0) {
        shortUrl.append(map[(num % 62).toInt() - 1])
        num /= 62
    }

    // Reverse shortURL to complete base conversion
    return shortUrl.reverse().toString()
}
