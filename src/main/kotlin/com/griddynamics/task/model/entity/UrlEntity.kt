package com.griddynamics.task.model.entity

import javax.persistence.*


@Entity
@Table(name = "url")
class UrlEntity(
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long?,
        val fullUrl: String,
        var shortenUrl: String?
)