package com.griddynamics.task.model.dto.request

data class RequestUrlDto(
        val fullUrl: String
)