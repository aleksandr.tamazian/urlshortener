package com.griddynamics.task.model.dto.response

data class ResponseUrlDto(
        val fullUrl: String,
        val shortenUrl: String
)