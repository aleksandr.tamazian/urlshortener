package com.griddynamics.task.repository

import com.griddynamics.task.model.entity.UrlEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param


@Repository
interface UrlRepository : JpaRepository<UrlEntity, Long> {
    fun findByShortenUrl(shortenUrl: String): UrlEntity

    @Modifying
    @Query("UPDATE UrlEntity u SET u.shortenUrl = :shortenUrl WHERE u.id = :id")
    fun updateShortenUrl(@Param(value = "id") id: Long, @Param(value = "shortenUrl") phone: String)
}