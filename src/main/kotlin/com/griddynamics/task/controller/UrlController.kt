package com.griddynamics.task.controller

import com.griddynamics.task.model.dto.request.RequestUrlDto
import com.griddynamics.task.model.dto.response.ResponseUrlDto
import com.griddynamics.task.service.UrlService
import org.springframework.web.bind.annotation.*

@RestController()
@RequestMapping("/url-shortener")
class UrlController(
        private val urlService: UrlService
) {
    @GetMapping("/retrieve/{shortenUrl}")
    fun retrieveFullUrl(@PathVariable shortenUrl: String): ResponseUrlDto {
        return urlService.retrieveFullUrl(shortenUrl)
    }

    @PostMapping("/create")
    fun shortenUrl(@RequestBody requestUrlDto: RequestUrlDto): ResponseUrlDto {
        return urlService.addUrl(requestUrlDto)
    }
}