CREATE TABLE url
(
    id          BIGINT  NOT NULL GENERATED ALWAYS AS IDENTITY,
    full_url    VARCHAR NOT NULL,
    shorten_url VARCHAR NULL,
    PRIMARY KEY (id)
);

-- Default index created for constraint
ALTER TABLE url
    ADD CONSTRAINT uniquecfull_url_const UNIQUE (full_url);

CREATE INDEX idx_shorten_url ON url (shorten_url);
