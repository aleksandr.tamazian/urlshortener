package com.griddynamics.task

import com.griddynamics.task.model.dto.request.RequestUrlDto
import com.griddynamics.task.repository.UrlRepository
import com.griddynamics.task.service.UrlService
import com.griddynamics.task.tools.shortenUrl
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@ActiveProfiles("test")
@SpringBootTest
class UrlShortenerTest {

    @Autowired
    private lateinit var urlService: UrlService

    @Autowired
    private lateinit var urlRepository: UrlRepository

    @BeforeEach
    fun cleanup() {
        urlRepository.deleteAll()
    }

    @Test
    @Order(1)
    fun shouldShortenUrl() {
        val responseDto = urlService.addUrl(RequestUrlDto("http://www.apple.com/iphone"))
        var shortenUrl = shortenUrl(1)
        assert(shortenUrl == responseDto.shortenUrl)

        val urlWithSpaces = "   http://www.apple.com/ipad   "
        val responseDtoWithSpaces = urlService.addUrl(RequestUrlDto(urlWithSpaces))
        shortenUrl = shortenUrl(2)
        assert(shortenUrl == responseDtoWithSpaces.shortenUrl)
        assert(urlWithSpaces.trim() == responseDtoWithSpaces.fullUrl)
    }

    @Test
    @Order(2)
    fun shouldNotAddDuplicatedUrl() {
        val firstUrl = "http://www.apple.com/ipod"
        val secondUrl = "http://www.apple.com/macbook"
        val executor = Executors.newFixedThreadPool(4)

        executor.submit { urlService.addUrl(RequestUrlDto(firstUrl)) }
        executor.submit { urlService.addUrl(RequestUrlDto(secondUrl)) }
        executor.submit { urlService.addUrl(RequestUrlDto(firstUrl)) }
        executor.submit { urlService.addUrl(RequestUrlDto(secondUrl)) }

        executor.awaitTermination(500, TimeUnit.MILLISECONDS)

        assert(urlRepository.findAll().size == 2)
    }
}
